package main

import (
	"fmt"
	"encoding/csv"
	"os"
	"time"
	"strconv"
	"io"
	"sort"
)

const BBRepository = "https://bitbucket.org/sergeysergeev/task5"

type LogItem struct {
	Time       time.Time
	SrcUser    string
	SrcIp      string
	SrcPort    string
	DestUsr    string
	DestIp     string
	DestPort   string
	InputByte  int
	OutputByte int
}

func (l LogItem) GetChar() string {
	return fmt.Sprintf("%s %s %s %s", l.SrcUser, l.SrcPort, l.DestIp, l.DestPort)
}

func getNewLogItem(r *csv.Reader) (*LogItem, error) {
	in, err := r.Read()
	if err != nil {
		return nil, err
	}
	parseTime, err := time.Parse("2006-01-02T15:04:05.999-0700", in[0])
	if err != nil {
		return nil, err
	}
	inByte, err := strconv.Atoi(in[7])
	if err != nil {
		return nil, err
	}
	outByte, err := strconv.Atoi(in[8])
	if err != nil {
		return nil, err
	}
	// [0]_time [1]src_user [2]src_ip [3]src_port [4]dest_user [5]dest_ip [6]dest_port [7]input_byte [8]output_byte
	return &LogItem{
		Time:       parseTime,
		SrcUser:    in[1],
		SrcIp:      in[2],
		SrcPort:    in[3],
		DestUsr:    in[4],
		DestIp:     in[5],
		DestPort:   in[6],
		InputByte:  inByte,
		OutputByte: outByte,
	}, err
}

type PeriodicalCounter struct {
	PeriodMap map[string]Periodical
}

func InitPeriodicalCounter() *PeriodicalCounter {
	return &PeriodicalCounter{
		make(map[string]Periodical),
	}
}

func (p *PeriodicalCounter) CheckElem(l LogItem, hashKey string) {
	key, isExist := p.PeriodMap[hashKey] //l.SrcUser
	if !isExist {
		key.PrevTime = l.Time
		key.IsPeriodical = false
		key.WasTwo = false
	} else {
		if key.IsPeriodical || !key.WasTwo {
			if !key.WasTwo {
				key.WasTwo = true
				key.PrevPrevTime = key.PrevTime
				key.PrevTime = l.Time
				key.IsPeriodical = true
				key.Period = int64(key.PrevPrevTime.Sub(key.PrevTime).Seconds())
			} else {
				if key.Period == int64(l.Time.Sub(key.PrevTime).Seconds()) {
					key.PrevPrevTime = key.PrevTime
					key.PrevTime = l.Time
				} else {
					key.IsPeriodical = false
				}
			}
		}
	}
	p.PeriodMap[l.SrcUser] = key
}

func (p *PeriodicalCounter) PrintPeriodical(out *os.File) {
	f := false
	for key, val := range p.PeriodMap {
		if val.IsPeriodical {
			f = true
			out.WriteString(key + "\n")
		}
	}
	if !f {
		out.WriteString("Переодичные запросы отсутствуют\n")
	}
}

type Periodical struct {
	PrevTime     time.Time
	PrevPrevTime time.Time
	Period       int64 //second
	WasTwo       bool
	IsPeriodical bool
}

type NGramm struct {
	len   int
	slice []string
	ngMap map[string]int64
}

func InitNGram(count int) *NGramm {
	return &NGramm{
		count,
		make([]string, 0, count),
		make(map[string]int64),
	}
}

func (n *NGramm) AddItem(l LogItem) {
	if len(n.slice) != n.len {
		n.slice = append(n.slice, l.GetChar())
	} else {
		for i := 0; i < n.len-1; i++ {
			n.slice[i] = n.slice[i+1]
		}
		n.slice[n.len-1] = l.GetChar()
	}
	if len(n.slice) == n.len {
		var res string
		for i, val := range n.slice {
			res += fmt.Sprintf("%s", val)
			if i != (n.len - 1) {
				res += "|"
			}
		}
		n.ngMap[res]++
	}
}

type PopularNGramm struct {
	Ngram string
	Count int64
}

func (n *NGramm) getPopular() []PopularNGramm {
	var popular []PopularNGramm
	for key, val := range n.ngMap {
		popular = append(popular, PopularNGramm{key, val})
	}
	sort.SliceStable(popular, func(i, j int) bool {
		if popular[i].Count > popular[j].Count {
			return true
		}
		return false
	})
	var res []PopularNGramm
	for i := 0; i < 5 && i < len(popular); i++ {
		res = append(res, popular[i])
	}
	return res
}

func main() {
	file, err := os.Open("shkib.csv")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	out, err := os.Create("out.txt")
	if err != nil {
		panic(err)
	}
	defer out.Close()
	// [0]_time [1]src_user [2]src_ip [3]src_port [4]dest_user [5]dest_ip [6]dest_port [7]input_byte [8]output_byte
	r := csv.NewReader(file)
	str, err := r.Read()
	fmt.Println(str, err)
	user := make(map[string]int64)
	data := make(map[string]int64)
	srcUserPeriod := InitPeriodicalCounter()
	srcIpPeriod := InitPeriodicalCounter()
	n3gramm := InitNGram(3)
	n4gramm := InitNGram(4)
	n5gramm := InitNGram(5)

	for {
		l, err := getNewLogItem(r)
		if err == io.EOF {
			fmt.Println("end")
			break
		}
		user[l.SrcUser]++
		data[l.SrcUser] += int64(l.OutputByte)
		n3gramm.AddItem(*l)
		n4gramm.AddItem(*l)
		n5gramm.AddItem(*l)
		srcUserPeriod.CheckElem(*l, l.SrcUser)
		srcIpPeriod.CheckElem(*l, l.SrcIp)
	}
	out.WriteString(fmt.Sprintf("#Исходный код в репозитории %s \n", BBRepository))
	out.WriteString("#Поиск 5ти пользователей, сгенерировавших наибольшее количество запросов\n")
	maxInHash(user, out)
	out.WriteString("#Поиск 5ти пользователей, отправивших наибольшее количество данных\n")
	maxInHash(data, out)
	out.WriteString("#Поиск регулярных запросов (запросов выполняющихся периодически) по полю src_user\n")
	srcUserPeriod.PrintPeriodical(out)
	out.WriteString("#Поиск регулярных запросов (запросов выполняющихся периодически) по полю src_ip\n")
	srcIpPeriod.PrintPeriodical(out)
	out.WriteString("#Рассматривая события сетевого трафика как символы неизвестного языка, найти 5 наиболее устойчивых N-грамм журнала событий (текста на неизвестном языке), (https://ru.wikipedia.org/wiki/N-грамм), где N=3-5. Тип символа задается квартетом user+src_port+dest_ip+dest_port.\n")
	popNgr := n3gramm.getPopular()
	popNgr = append(popNgr, n4gramm.getPopular()...)
	popNgr = append(popNgr, n5gramm.getPopular()...)
	sort.SliceStable(popNgr, func(i, j int) bool {
		if popNgr[i].Count > popNgr[j].Count {
			return true
		}
		return false
	})
	for i := 0; i < 5 && i < len(popNgr); i++ {
		out.WriteString(popNgr[i].Ngram + "\n")
	}
}

type CountHash struct {
	User  string
	count int64
}

func maxInHash(arr map[string]int64, out *os.File) {
	var mass []CountHash
	for key, val := range arr {
		mass = append(mass, CountHash{key, val})
	}
	sort.SliceStable(mass, func(i, j int) bool {
		if mass[i].count > mass[j].count {
			return true
		}
		return false
	})
	for _, val := range mass[0:5] {
		out.Write([]byte(val.User))
		out.Write([]byte("\n"))
	}
}
